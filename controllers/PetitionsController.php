<?php

namespace app\controllers;

use Yii;
use app\models\Petitions;
use app\models\PetitionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\models\Registers;
use app\models\Approves;

/**
 * PetitionsController implements the CRUD actions for Petitions model.
 */
class PetitionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Petitions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PetitionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->OrderBy(['id' => SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Petitions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Petitions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Petitions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // set message tot notify 
            
            $message = 'เรื่อง '.$model->title.' ความเร่งด่วน:'.$model->getImportantName();            
            $this->sendToLine($message);
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * ลงทะเบียนรับเรื่อง.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegist($id)
    {
        $model = new Registers();

        $model->petition_id = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->save();

        $petition = $this->findModel($id);
        $petition->req_status = '1';
        $petition->save();

        $searchModel = new PetitionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->redirect(['index']);
    }

    /**
     * อนุมัติ.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApprove($id)
    {
        $model = new Approves();

        $model->petition_id = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->save();

        $petition = $this->findModel($id);
        $petition->req_status = '2';
        $petition->save();

        $searchModel = new PetitionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->redirect(['index']);
    }

    /**
     * อนุมัติ.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionJobdone($id)
    {
        $petition = $this->findModel($id);
        $petition->req_status = '3';
        $petition->save();

        $searchModel = new PetitionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Petitions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Petitions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('_print', [
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper formats
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ใบบันทึกข้อความ'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }


    /**
     * Finds the Petitions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Petitions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Petitions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function sendToLine($message){    

        $line_api = 'https://notify-api.line.me/api/notify';
        // Token RM-IM Group
        $line_token = 'ppA4vpYIk6dBnpN1nS38xJhkG4uii75gXVb62LHyS9y';
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://notify-api.line.me/api/notify");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'message='.$message);
        // follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Bearer '.$line_token,
        ]);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        $server_output = curl_exec ($ch);
    
        curl_close ($ch);
    }

}
