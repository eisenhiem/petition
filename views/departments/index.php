<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กลุ่มงาน/แผนก';
?>
<div class="departments-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('เพิ่มกลุ่มงาน/แผนก', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'dep_id',
            'dep_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
