<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Departments */

$this->title = $model->dep_id;
\yii\web\YiiAsset::register($this);
?>
<div class="departments-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dep_id',
            'dep_name',
        ],
    ]) ?>
    <hr>
    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->dep_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->dep_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจที่จะลบกลุ่มงานหรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
