<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OfficeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="office-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'office_id') ?>

    <?= $form->field($model, 'office_name') ?>

    <?= $form->field($model, 'office_address') ?>

    <?= $form->field($model, 'office_postcode') ?>

    <?= $form->field($model, 'office_phone_number') ?>

    <?php // echo $form->field($model, 'office_email') ?>

    <?php // echo $form->field($model, 'office_fax_number') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('ยกเลิก', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
