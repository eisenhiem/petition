<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Office */

$this->title = $model->office_id;
\yii\web\YiiAsset::register($this);
?>
<div class="office-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'office_id',
            'office_name',
            'office_address',
            'office_postcode',
            'office_phone_number',
            'office_email:email',
            'office_fax_number',
        ],
    ]) ?>
    <hr>
    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->office_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->office_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจที่จะลบข้อมูลหน่วยงานใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
