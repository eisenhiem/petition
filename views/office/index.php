<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OfficeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน่วยงาน';
?>
<div class="office-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('เพิ่มหน่วยงาน', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        //    'office_id',
            'office_name',
            'office_address',
            'office_postcode',
            'office_phone_number',
            'office_email:email',
            'office_fax_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
