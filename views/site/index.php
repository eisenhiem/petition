<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="card">
                <p align="center">
                    <?= Html::a("<img class='card-img-top' src='../web/images/register-icon.png' alt='Card image cap'><br>".'เขียนคำร้อง',
                        ['petitions/create'], ['class' => 'btn btn-lg btn-outline-primary','style' => 'border-radius: 20px']) 
                    ?>
                </p>
                <p class="card-text"> เขียนคำร้องเพื่อขอ เพิ่ม/ลบ/แก้ไข/ขอรายงาน/ส่งออกข้อมูล ในระบบสารสนเทศ</p>
                <p align="center"><?= Html::a('รายการคำร้องทั้งหมด', ['petitions/index'], ['class' => 'btn btn-success','style' => ['width'=>'200px']]) ?></p>
            </div>
        </div>
    </div>

</div>
