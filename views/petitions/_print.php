<?php

use app\models\Office;
use yii\helpers\Html;

$hospital = Office::findOne(1);
?>
<table width="100%">
    <tr>
        <td width="260">
            <?=Html::img(Yii::getAlias('@app').'/web/images/crut.png', ['width' => 60])?>
        </td>
        <td valign="bottom"><h3>บันทึกข้อความ</h3><br>
        </td>
    </tr>
</table>
<b>ส่วนราชการ </b><?= $model->getDepName() ?> <?= $hospital->office_name ?><br>

<table width="100%">
    <tr><td width="50%"  height="30"><b>ที่</b> อบ 0033.001.31/</td><td>วันที่ <?= $model->getRegisterDate() ?></td></tr>
    <tr><td ></td><td></td></tr>
</table>
    <b>เรื่อง</b> <?= $model->title ?> <br>
    <p>
    &emsp;&emsp;&emsp;&emsp;<?= $model->detail ?>
    </p>
    <b>วันที่ต้องการให้แล้วเสร็จ </b><?= $model->getFinishDate() ?>
    <br>
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="50%" style="vertical-align:top">
            &emsp;&emsp;จึงเรียนมาเพื่อโปรดพิจารณา<br><br>
            <p>
            &emsp;&emsp;&emsp;&emsp;ลงชื่อ&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ผู้ยื่นคำร้อง<br><br>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(<?= $model->request_by ?>)
            </p>
        </td>
        <td width="50%" style="vertical-align:top">
            <h4></h4><br>
            <p>
            <p>ลงชื่อ&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;หัวหน้ากลุ่มงาน/แผนก<br><br>
            &emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)
            </p>
        </td>
    </tr>
</table>
<sethtmlpagefooter name="sign" value="on"/>
<htmlpagefooter name="sign">
<table class="table_bordered" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%" style="vertical-align:top">
        <h4>ผู้รับคำร้อง</h4><br>
            <p>&emsp;&emsp;&emsp;&emsp;ลงชื่อ<br>
            &emsp;&emsp;&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)<br><br>
            &emsp;&emsp;&emsp;&emsp;&emsp; <br>
            </p>
        </td>
        <td width="50%" style="vertical-align:top" rowspan="3">
            <h4>การดำเนินการ</h4><br>
            <p>
            &emsp;เรื่อง <?= $model->title ?> ได้ทำการ<?= $model->is_insert == 1 ? 'เพิ่มข้อมูล ':'' ?>
            <?= $model->is_edit == 1 ? 'แก้ไขข้อมูล ':'' ?>
            <?= $model->is_delete == 1 ? 'ลบข้อมูล ':'' ?>
            <?= $model->is_report == 1 ? 'สร้างรายงาน ':'' ?>
            <?= $model->is_export == 1 ? 'ส่งออกข้อมูล ':'' ?>เรียบร้อยแล้ว <br>เมื่อวันที่ <u>&emsp;&emsp;/&emsp;&emsp;&emsp;/</u>25<u>&emsp; .</u><br><br>
            </p>
            <p>&emsp;จึงเรียนมาเพื่อโปรดทราบ<br><br>
            </p>
            <p>&emsp;&emsp;&emsp;&emsp;ลงชื่อ<br>
            &emsp;&emsp;&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)<br><br>
            &emsp;&emsp;&emsp;&emsp;&emsp;งานสารสนเทศทางการแพทย์<br><br>
            </p>
            <h4>&emsp;&emsp;ทราบ</h4><br>
            <p>&emsp;&emsp;&emsp;&emsp;ลงชื่อ<br>
            &emsp;&emsp;&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)<br><br>
            &emsp;&emsp; หัวหน้า<?= $model->getDepName() ?>
            </p>
        </td>
    </tr>
    <tr>
        <td width="50%" style="vertical-align:top">
            <h4>เรียนผู้อำนวยการ</h4>
            <p>
            &emsp;[&emsp;] เห็นควรอนุญาต&emsp;[&emsp;] เห็นควรไม่อนุญาต<br>
            <br>เพราะ<u>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;.</u><br><br>
            </p>
            <p>&emsp;&emsp;&emsp;&emsp;ลงชื่อ<br>            &emsp;&emsp;&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)<br><br>
            &emsp;&emsp; งานสารสนเทศทางการแพทย์<br>
            </p>
        </td>
    </tr>
    <tr>
        <td width="50%">
          <h4>ทราบ</h4>
          <p>
          &emsp;[&emsp;] ให้ดำเนินการได้&emsp;[&emsp;] ไม่ควรดำเนินการ<br>
          <br>เนื่องจาก<u>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;.</u> 
          <br><br>
          </p>
          <p>&emsp;&emsp;&emsp;&emsp;ลงชื่อ<br>
            &emsp;&emsp;&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)<br><br>
            &emsp;&emsp;&emsp;&emsp;ผู้อำนวยการ<?= $hospital->office_name ?><br>
          </p>
        </td>
    </tr>
    <tr>
    <td colspan="2">
        <b>หมายเหตุ</b>: การขอเพิ่ม/แก้ไข/ลบ/ขอรายงาน ถ้ามีเอกสารประกอบขอให้แนบมาด้วย<br>
        &emsp;&emsp;&emsp;&emsp; : ถ้าต้องการด่วนที่สุดให้แจ้งงานสารสนเทศทางการแพทย์ ทราบก่อนเสมอ
    </td>
    </tr>
</table>
</htmlpagefooter>