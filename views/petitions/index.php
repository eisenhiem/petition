<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PetitionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการคำร้อง';
?>
<div class="petitions-index">

    <h3 align="center"><?= Html::encode($this->title) ?></h3>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'before' => ' '
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'detail:ntext',
            [
                'attribute' => 'register_date',
                'value' => function($model) { return $model->getRegisterDate();},
            ],
            [
                'attribute' => 'is_urgent',
                'value' => function($model) { return $model->getImportantName();},
            ],
            [
                'attribute' => 'request_finish_date',
                'value' => function($model) { return $model->request_finish_date ? $model->getFinishDate():'ไม่ระบุ';},
            ],
            //'departments.dep_name',
            'request_by',
            [
                'attribute' => 'req_status',
                'value' => function($model) { return $model->getStatusName();},
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'รายละเอียด',
                'options'=>['style'=>'width:100px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['view','id'=>$model->id],['class' => 'btn btn-info']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ผู้รับ',
                'options'=>['style'=>'width:80px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{register}',
                'buttons'=>[
                    'register' => function($url,$model,$key){
                        return (!Yii::$app->user->isGuest && $model->req_status == 0) ? Html::a('รับเรื่อง',['regist','id'=>$model->id],['class' => 'btn btn-success'])
                        :'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ผู้อนุมัติ',
                'options'=>['style'=>'width:80px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{approve}',
                'buttons'=>[
                    'approve' => function($url,$model,$key){
                        return (!Yii::$app->user->identity->role <= 2 && $model->req_status == 1) ? Html::a('อนุมัติ',['approve','id'=>$model->id],['class' => 'btn btn-success']):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ผู้ดำเนินงาน',
                'options'=>['style'=>'width:80px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{done}',
                'buttons'=>[
                    'done' => function($url,$model,$key){
                        return (!Yii::$app->user->identity->role <= 2 && $model->req_status == 2) ? Html::a('เสร็จงาน',['jobdone','id'=>$model->id],['class' => 'btn btn-success']):'';
                    }
                ]
            ],
        ],
    ]); 
?>
</div>