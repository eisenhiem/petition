<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Petitions */

$this->title = 'เขียนคำร้อง';
?>
<div class="petitions-create">
    <h3 align="center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
