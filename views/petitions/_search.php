<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PetitionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="petitions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'register_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'register_date', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่ยื่นคำร้อง'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'is_urgent')->radioList($model->getItemImportant()) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'request_by') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'req_status')->radioList($model->getItemStatus()) ?>
        </div>
    </div>



    <?php // echo $form->field($model, 'is_insert') ?>

    <?php // echo $form->field($model, 'is_edit') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <?php // echo $form->field($model, 'is_report') ?>

    <?php // echo $form->field($model, 'is_export') ?>

    <?php // echo $form->field($model, 'request_finish_date') ?>

    <?php // echo $form->field($model, 'dep_id') ?>



    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        &emsp;&emsp;
        <?= Html::resetButton('ยกเลิก', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
