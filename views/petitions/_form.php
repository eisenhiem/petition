<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\date\DatePicker;

use app\models\Departments;
$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $model app\models\Petitions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="petitions-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <b>ประเภทคำร้อง</b>
        </div>
        <div class="col-md-1 col-sm-2">
            <?= $form->field($model, 'is_insert')->checkbox([ '0', '1']) ?>
        </div>
        <div class="col-md-1 col-sm-2">
            <?= $form->field($model, 'is_edit')->checkbox([ '0', '1']) ?>
        </div>
        <div class="col-md-1 col-sm-2">
            <?= $form->field($model, 'is_delete')->checkbox([ '0', '1']) ?>
        </div>
        <div class="col-md-1 col-sm-2">
            <?= $form->field($model, 'is_report')->checkbox([ '0', '1']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'is_export')->checkbox([ '0', '1']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'is_urgent')->radioList([ 2 => 'ด่วนมาก', 1 => ' ด่วน ', 0 => ' ปกติ ', ], ['prompt' => '']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'request_finish_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'request_finish_date', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่ต้องการ'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>'']) ?>        
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'request_by')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?php //= $form->field($model, 'req_status')->dropDownList([ 2 => '2', 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
