<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Petitions */

$this->title = $model->title;
\yii\web\YiiAsset::register($this);
?>
<div class="petitions-view">

    <h3> ใบคำร้องเลขที่ : <?= $model->id ?> </h3>
    วันที่ <?= $model->getRegisterDate() ?> <br>
    <b>เรื่อง</b> <?= $model->title ?> <br>
    <p>
    <b>รายละเอียด</b> <?= $model->detail ?> <br>
    </p>
    <b>ประเภทการขอ</b>
    <?= $model->is_insert == 1 ? ' ขอเพิ่มข้อมูล':'' ?>
    <?= $model->is_edit == 1 ? ' ขอแก้ไขข้อมูล':'' ?>
    <?= $model->is_delete == 1 ? ' ขอลบข้อมูล':'' ?>
    <?= $model->is_report == 1 ? ' ขอรายงาน':'' ?>
    <?= $model->is_export == 1 ? ' ส่งออกข้อมูล':'' ?>
    <br>
    <b>ความเร่งด่วน </b> <?= $model->getImportantName() ?><br>
    <b>วันที่ต้องการให้แล้วเสร็จ </b><?= $model->getFinishDate() ?> <br>
    <b>ผู้ยื่นคำร้อง </b> <?= $model->request_by ?><br>
    <b>กลุ่มงาน/แผนก </b><?= $model->getDepName() ?><br>
    <b>สถานะใบคำร้อง </b> <?= $model->getStatusName() ?><br>
    <hr>
    <p>
        <?= Html::a('พิมพ์ใบคำร้อง', ['print', 'id' => $model->id], ['target' => '_blank','class' => 'btn btn-primary']) ?>
        &emsp;&emsp;
        <?= Html::a('แก้ไขใบคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        &emsp;&emsp;
        <?= Html::a('ยกเลิกใบคำร้อง', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าจะยกเลิกใบคำร้องนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
