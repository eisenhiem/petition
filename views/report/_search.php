<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportSummarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-summary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ym') ?>

    <?= $form->field($model, 'total_add') ?>

    <?= $form->field($model, 'total_edit') ?>

    <?= $form->field($model, 'total_delete') ?>

    <?= $form->field($model, 'total_report') ?>

    <?php // echo $form->field($model, 'total_export') ?>

    <?php // echo $form->field($model, 'total') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
