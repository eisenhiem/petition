<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานสรุป';
?>
<div class="report-summary-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ym',
            'total_add',
            'total_edit',
            'total_delete',
            'total_report',
            'total_export',
            'total',
        ],
    ]); 
?>
</div>