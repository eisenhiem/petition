<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DirectorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้อำนวยการ';
?>
<div class="directors-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('เพิ่มผู้อำนวยการ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'director_id',
            'director_name',
            'director_position1',
            'director_position2',
            'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
