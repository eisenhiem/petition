<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Directors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="directors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'director_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director_position1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director_position2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
