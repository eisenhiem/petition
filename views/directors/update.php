<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directors */

$this->title = 'แก้ไขข้อมูลผู้อำนวยการ: ' . $model->director_id;
?>
<div class="directors-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
