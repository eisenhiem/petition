<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Directors */

$this->title = $model->director_id;
\yii\web\YiiAsset::register($this);
?>
<div class="directors-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'director_id',
            'director_name',
            'director_position1',
            'director_position2',
            'is_active',
        ],
    ]) ?>
    <hr>
    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->director_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->director_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจที่จะลบรายชื่อผู้อำนวยการคนนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
