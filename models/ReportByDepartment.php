<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_by_department".
 *
 * @property string|null $ym
 * @property int $dep_id
 * @property string|null $department_name
 * @property int $total_add
 * @property int $total_edit
 * @property int $total_delete
 * @property int $total_report
 * @property int $total_export
 * @property int $total
 */
class ReportByDepartment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_by_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id','total_add', 'total_edit', 'total_delete', 'total_report', 'total_export', 'total'], 'integer'],
            [['ym'], 'string', 'max' => 7],
            [['department_name'], 'string', 'max' => 255],
        ];
    }

    public static function primaryKey()
    {
        return ['ym','dep_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ym' => 'ปี-เดือน',
            'dep_id' => 'รหัสกลุ่มงาน',
            'department_name' => 'กลุ่มงาน/แผนก',
            'total_add' => 'ขอเพิ่ม',
            'total_edit' => 'ขอแก้ไข',
            'total_delete' => 'ขอลบ',
            'total_report' => 'ขอรายงาน',
            'total_export' => 'ส่งออกข้อมูล',
            'total' => 'รวม',
        ];
    }
}
