<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportByStatus;

/**
 * ReportByStatusSearch represents the model behind the search form of `app\models\ReportByStatus`.
 */
class ReportByStatusSearch extends ReportByStatus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ym'], 'safe'],
            [['pending', 'register', 'approve', 'finish', 'total'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportByStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pending' => $this->pending,
            'register' => $this->register,
            'approve' => $this->approve,
            'finish' => $this->finish,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'ym', $this->ym]);

        return $dataProvider;
    }
}
