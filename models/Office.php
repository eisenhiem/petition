<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property int $office_id
 * @property string $office_name
 * @property string|null $office_address
 * @property string|null $office_postcode
 * @property string|null $office_phone_number
 * @property string|null $office_email
 * @property string|null $office_fax_number
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['office_name'], 'required'],
            [['office_name', 'office_address', 'office_email'], 'string', 'max' => 255],
            [['office_postcode'], 'string', 'max' => 10],
            [['office_phone_number'], 'string', 'max' => 30],
            [['office_fax_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'office_id' => 'Office ID',
            'office_name' => 'ชื่อหน่วยงาน',
            'office_address' => 'ที่อยู่',
            'office_postcode' => 'รหัสไปรษณีย์',
            'office_phone_number' => 'เบอร์โทรศัพท์',
            'office_email' => 'E-mail',
            'office_fax_number' => 'เบอร์โทรสาร',
        ];
    }
}
