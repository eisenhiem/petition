<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_by_status".
 *
 * @property string|null $ym
 * @property int $pending
 * @property int $register
 * @property int $approve
 * @property int $finish
 * @property int $total
 */
class ReportByStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_by_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pending', 'register', 'approve', 'finish', 'total'], 'integer'],
            [['ym'], 'string', 'max' => 7],
        ];
    }

    public static function primaryKey()
    {
        return ['ym'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ym' => 'ปี-เดือน',
            'pending' => 'รอรับ',
            'register' => 'รับเรื่อง',
            'approve' => 'อนุมัติ',
            'finish' => 'เสร็จ',
            'total' => 'รวม',
        ];
    }
}
