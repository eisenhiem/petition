<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportSummary;

/**
 * ReportSummarySearch represents the model behind the search form of `app\models\ReportSummary`.
 */
class ReportSummarySearch extends ReportSummary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ym'], 'safe'],
            [['total_add', 'total_edit', 'total_delete', 'total_report', 'total_export', 'total'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportSummary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'total_add' => $this->total_add,
            'total_edit' => $this->total_edit,
            'total_delete' => $this->total_delete,
            'total_report' => $this->total_report,
            'total_export' => $this->total_export,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'ym', $this->ym]);

        return $dataProvider;
    }
}
