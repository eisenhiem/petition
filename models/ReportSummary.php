<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_summary".
 *
 * @property string|null $ym
 * @property int $total_add
 * @property int $total_edit
 * @property int $total_delete
 * @property int $total_report
 * @property int $total_export
 * @property int $total
 */
class ReportSummary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_add', 'total_edit', 'total_delete', 'total_report', 'total_export', 'total'], 'integer'],
            [['ym'], 'string', 'max' => 7],
        ];
    }

    public static function primaryKey()
    {
        return ['ym'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ym' => 'ปี-เดือน',
            'total_add' => 'ขอเพิ่ม',
            'total_edit' => 'ขอแก้ไข',
            'total_delete' => 'ขอลบ',
            'total_report' => 'ขอรายงาน',
            'total_export' => 'ส่งออกข้อมูล',
            'total' => 'รวม',
        ];
    }
}
