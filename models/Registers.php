<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registers".
 *
 * @property int $petition_id
 * @property int $user_id
 * @property string|null $register_date
 */
class Registers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['petition_id', 'user_id'], 'required'],
            [['petition_id', 'user_id'], 'integer'],
            [['register_date'], 'safe'],
            [['petition_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'petition_id' => 'Petition ID',
            'user_id' => 'User ID',
            'register_date' => 'Register Date',
        ];
    }
}
