<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Petitions;

/**
 * PetitionsSearch represents the model behind the search form of `app\models\Petitions`.
 */
class PetitionsSearch extends Petitions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dep_id'], 'integer'],
            [['title', 'detail', 'register_date', 'is_urgent', 'is_insert', 'is_edit', 'is_delete', 'is_report', 'is_export', 'request_finish_date', 'request_by', 'req_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Petitions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'request_finish_date' => $this->request_finish_date,
            'dep_id' => $this->dep_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'is_urgent', $this->is_urgent])
            ->andFilterWhere(['like', 'is_insert', $this->is_insert])
            ->andFilterWhere(['like', 'is_edit', $this->is_edit])
            ->andFilterWhere(['like', 'is_delete', $this->is_delete])
            ->andFilterWhere(['like', 'is_report', $this->is_report])
            ->andFilterWhere(['like', 'is_export', $this->is_export])
            ->andFilterWhere(['like', 'request_by', $this->request_by])
            ->andFilterWhere(['like', 'req_status', $this->req_status]);

        return $dataProvider;
    }
}
