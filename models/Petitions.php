<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "petitions".
 *
 * @property int $id
 * @property string $title เรื่อง
 * @property string $detail รายละเอียด
 * @property string $register_date วันที่
 * @property string $is_urgent ความเร่งด่วน
 * @property string $is_insert เพิ่ม
 * @property string $is_edit แก้ไข
 * @property string $is_delete ลบ
 * @property string $is_report รายงาน
 * @property string $is_export ส่งออกข้อมูล
 * @property string|null $request_finish_date วันที่ต้องการให้แล้วเสร็จ
 * @property int|null $dep_id กลุ่มงาน
 * @property string $request_by ผู้ยื่นคำร้อง
 * @property string|null $req_status สถานะคำร้อง
 */
class Petitions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const STATUS_PENDING = 0;
    const STATUS_REGISTER = 1;
    const STATUS_APPROVE = 2;
    const STATUS_DONE = 3;

    public static function tableName()
    {
        return 'petitions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'detail', 'request_by'], 'required'],
            [['detail', 'is_urgent', 'is_insert', 'is_edit', 'is_delete', 'is_report', 'is_export', 'req_status'], 'string'],
            [['register_date', 'request_finish_date'], 'safe'],
            [['dep_id'], 'integer'],
            [['title', 'request_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'เลขที่',
            'title' => 'เรื่อง',
            'detail' => 'รายละเอียด',
            'register_date' => 'วันที่',
            'is_urgent' => 'ความเร่งด่วน',
            'is_insert' => 'เพิ่ม',
            'is_edit' => 'แก้ไข',
            'is_delete' => 'ลบ',
            'is_report' => 'รายงาน',
            'is_export' => 'ขอข้อมูล',
            'request_finish_date' => 'วันที่ต้องการแล้วเสร็จ',
            'dep_id' => 'กลุ่มงาน',
            'request_by' => 'ผู้ยื่นคำขอ',
            'req_status' => 'สถานะ',
        ];
    }

    /**
     * Gets กลุ่มงาน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasOne(Departments::className(), ['dep_id' => 'dep_id']);
    }

    public function getDepName()
    {
        return $this->departments->dep_name;
    }

    public static function itemsAlias($key){

        $items = [
            'status'=>[
                self::STATUS_PENDING => 'รอลงรับ',
                self::STATUS_REGISTER => 'รับเรื่องแล้ว',
                self::STATUS_APPROVE => 'อนุมัติแล้ว',
                self::STATUS_DONE => 'เสร็จงาน'
            ],
            'important' => [
                '0' => 'ปกติ',
                '1' => 'ด่วน',
                '2' => 'ด่วนที่สุด',
            ]            
        ];
        return ArrayHelper::getValue($items,$key,[]);
    }

    public function getItemStatus(){
        return self::itemsAlias('status');
    }

    public function getItemImportant(){
        return self::itemsAlias('important');
    }

    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemStatus(),$this->req_status);
    }

    public function getImportantName(){
        return ArrayHelper::getValue($this->getItemImportant(),$this->is_urgent);
    }

    public function getThaiDate($date){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        $thday = intVal(substr($date,8,2));
        return $thday.' '.$thmonth.' พ.ศ. '.$thyear; 
    }

    public function getRegisterDate(){
        return $this->getThaiDate($this->register_date);
    }

    public function getFinishDate(){
        return $this->getThaiDate($this->request_finish_date);
    }

}
