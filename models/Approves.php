<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "approves".
 *
 * @property int $petition_id
 * @property int $user_id
 * @property string $approve_status
 * @property string|null $approve_date
 */
class Approves extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'approves';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['petition_id', 'user_id'], 'required'],
            [['petition_id', 'user_id'], 'integer'],
            [['approve_status'], 'string'],
            [['approve_date'], 'safe'],
            [['petition_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'petition_id' => 'Petition ID',
            'user_id' => 'User ID',
            'approve_status' => 'Approve Status',
            'approve_date' => 'Approve Date',
        ];
    }
}
