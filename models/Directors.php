<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directors".
 *
 * @property int $director_id
 * @property string|null $director_name
 * @property string|null $director_position1
 * @property string|null $director_position2
 * @property string|null $is_active
 */
class Directors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active'], 'string'],
            [['director_name', 'director_position1', 'director_position2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'director_id' => 'Director ID',
            'director_name' => 'ชื่อ-สกุล',
            'director_position1' => 'ตำแหน่ง 1',
            'director_position2' => 'ตำแหน่ง 2',
            'is_active' => 'สถานะ',
        ];
    }
}
