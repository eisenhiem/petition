/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : petition

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2021-07-01 10:12:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for `report_by_department`
-- ----------------------------
DROP VIEW IF EXISTS `report_by_department`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_by_department` AS select date_format(`petitions`.`register_date`,'%Y-%m') AS `ym`,`departments`.`dep_id` AS `dep_id`,`departments`.`dep_name` AS `department_name`,count((case `petitions`.`is_insert` when '1' then `petitions`.`id` end)) AS `total_add`,count((case `petitions`.`is_edit` when '1' then `petitions`.`id` end)) AS `total_edit`,count((case `petitions`.`is_delete` when '1' then `petitions`.`id` end)) AS `total_delete`,count((case `petitions`.`is_report` when '1' then `petitions`.`id` end)) AS `total_report`,count((case `petitions`.`is_export` when '1' then `petitions`.`id` end)) AS `total_export`,count(`petitions`.`id`) AS `total` from (`petitions` join `departments` on((`petitions`.`dep_id` = `departments`.`dep_id`))) group by date_format(`petitions`.`register_date`,'%Y-%m'),`departments`.`dep_name` ;

-- ----------------------------
-- View structure for `report_by_status`
-- ----------------------------
DROP VIEW IF EXISTS `report_by_status`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_by_status` AS select date_format(`petitions`.`register_date`,'%Y-%m') AS `ym`,count((case `petitions`.`req_status` when '0' then `petitions`.`id` end)) AS `pending`,count((case `petitions`.`req_status` when '1' then `petitions`.`id` end)) AS `register`,count((case `petitions`.`req_status` when '2' then `petitions`.`id` end)) AS `approve`,count((case `petitions`.`req_status` when '3' then `petitions`.`id` end)) AS `finish`,count(`petitions`.`id`) AS `total` from `petitions` group by date_format(`petitions`.`register_date`,'%Y-%m') ;

-- ----------------------------
-- View structure for `report_summary`
-- ----------------------------
DROP VIEW IF EXISTS `report_summary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_summary` AS select date_format(`petitions`.`register_date`,'%Y-%m') AS `ym`,count((case `petitions`.`is_insert` when '1' then `petitions`.`id` end)) AS `total_add`,count((case `petitions`.`is_edit` when '1' then `petitions`.`id` end)) AS `total_edit`,count((case `petitions`.`is_delete` when '1' then `petitions`.`id` end)) AS `total_delete`,count((case `petitions`.`is_report` when '1' then `petitions`.`id` end)) AS `total_report`,count((case `petitions`.`is_export` when '1' then `petitions`.`id` end)) AS `total_export`,count(`petitions`.`id`) AS `total` from `petitions` group by date_format(`petitions`.`register_date`,'%Y-%m') ;
