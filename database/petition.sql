/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : petition

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2021-06-28 20:34:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `approves`
-- ----------------------------
DROP TABLE IF EXISTS `approves`;
CREATE TABLE `approves` (
`petition_id`  int(11) NOT NULL ,
`user_id`  int(11) NOT NULL ,
`approve_status`  enum('9','1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '9' ,
`approve_date`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`petition_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of approves
-- ----------------------------
BEGIN;
INSERT INTO `approves` VALUES ('1', '1', '9', '2021-06-28 10:55:11');
COMMIT;

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
`dep_id`  int(2) NOT NULL AUTO_INCREMENT ,
`dep_name`  varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
PRIMARY KEY (`dep_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=15

;

-- ----------------------------
-- Records of departments
-- ----------------------------
BEGIN;
INSERT INTO `departments` VALUES ('1', 'กลุ่มงานการพยาบาล'), ('2', 'กลุ่มงานบริหารทั่วไป'), ('3', 'กลุ่มงานทันตกรรม'), ('4', 'องค์กรแพทย์'), ('5', 'กลุ่มงานประกันสุขภาพ ยุทธศาสตร์ฯ'), ('6', 'กลุ่มงานเทคนิคการแพทย์'), ('7', 'กลุ่มงานบริการด้านปฐมภูมิและองค์รวม'), ('8', 'กลุ่มงานเภสัชกรรมและคุ้มครองผู้บริโภค'), ('9', 'กลุ่มงานฟื้นฟูสมรรถภาพ'), ('10', 'กลุ่มงานแพทย์แผนไทยและแพทย์ทางเลือก'), ('11', 'กลุ่มการพยาบาล แผนกผู้ป่วยนอก'), ('12', 'กลุ่มการพยาบาล แผนกผู้ป่วยใน'), ('13', 'กลุ่มการพยาบาล แผนกผู้ป่วยอุบัติเหตุ-ฉุกเฉิน'), ('14', 'กลุ่มการพยาบาล แผนกซัพพลาย-ซักฟอก');
COMMIT;

-- ----------------------------
-- Table structure for `directors`
-- ----------------------------
DROP TABLE IF EXISTS `directors`;
CREATE TABLE `directors` (
`director_id`  int(11) NOT NULL AUTO_INCREMENT ,
`director_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`director_position1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`director_position2`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' ,
PRIMARY KEY (`director_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of directors
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `office`
-- ----------------------------
DROP TABLE IF EXISTS `office`;
CREATE TABLE `office` (
`office_id`  int(11) NOT NULL AUTO_INCREMENT ,
`office_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`office_address`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_postcode`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_phone_number`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_fax_number`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`office_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of office
-- ----------------------------
BEGIN;
INSERT INTO `office` VALUES ('1', 'โรงพยาบาลเหล่าเสือโก้ก', '298 หมู่ 6 ตำบลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี', '34000', '(045) 304 205, (045) 304 265', 'lsk.hospital@gmail.com', '(045) 304 206');
COMMIT;

-- ----------------------------
-- Table structure for `petitions`
-- ----------------------------
DROP TABLE IF EXISTS `petitions`;
CREATE TABLE `petitions` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'เรื่อง' ,
`detail`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด' ,
`register_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'วันที่' ,
`is_urgent`  enum('3','2','1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'ความเร่งด่วน' ,
`is_insert`  enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'เพิ่ม' ,
`is_edit`  enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'แก้ไข' ,
`is_delete`  enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'ลบ' ,
`is_report`  enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'รายงาน' ,
`is_export`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'ส่งออกข้อมูล' ,
`request_finish_date`  date NULL DEFAULT NULL COMMENT 'วันที่ต้องการให้แล้วเสร็จ' ,
`dep_id`  int(11) NULL DEFAULT NULL COMMENT 'กลุ่มงาน' ,
`request_by`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ผู้ยื่นคำร้อง' ,
`req_status`  enum('3','2','1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'สถานะคำร้อง' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of petitions
-- ----------------------------
BEGIN;
INSERT INTO `petitions` VALUES ('1', 'ทดสอบเขียนคำร้อง', 'อยากลองเขียนเฉยๆ', '2021-06-27 19:04:31', '0', '1', '0', '0', '0', '0', '2021-06-28', '2', 'นายจักรพงษ์ วงศ์กมลาไสย', '2'), ('2', 'ขอแก้ไขเลขที่ใบเสร็จในระบบ HIS', 'เนื่องจาก การเงินได้ทำการยกเลิกใบเสร็จจากระบบ ทั้งที่ออกใบเสร็จให้คนไข้แล้ว อยากให้เพิ่มใบเสร็จเดิมเข้าไปในระบบ', '2021-06-28 19:44:28', '2', '1', '0', '0', '0', '0', '2021-06-28', '2', 'นางสาวจินตนา บุญสุข', '0');
COMMIT;

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
`user_id`  int(11) NOT NULL ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`public_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`gravatar_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`gravatar_id`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`location`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`website`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`bio`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
`timezone`  varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci

;

-- ----------------------------
-- Records of profile
-- ----------------------------
BEGIN;
INSERT INTO `profile` VALUES ('1', 'ผู้ดูแลระบบ', 'lsk.hospital@gmail.com', null, null, null, null, '-', null), ('2', 'นายจักรพงษ์  วงศ์กมลาไสย', 'jwongkamalasai@gmail.com', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'นักวิชาการคอมพิวเตอร์ปฏิบัติการ', null);
COMMIT;

-- ----------------------------
-- Table structure for `registers`
-- ----------------------------
DROP TABLE IF EXISTS `registers`;
CREATE TABLE `registers` (
`petition_id`  int(11) NOT NULL ,
`user_id`  int(11) NOT NULL ,
`register_date`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`petition_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of registers
-- ----------------------------
BEGIN;
INSERT INTO `registers` VALUES ('1', '1', '2021-06-28 10:49:56');
COMMIT;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`password_hash`  varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`auth_key`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`confirmed_at`  int(11) NULL DEFAULT NULL ,
`unconfirmed_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`blocked_at`  int(11) NULL DEFAULT NULL ,
`registration_ip`  varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`created_at`  int(11) NOT NULL ,
`updated_at`  int(11) NOT NULL ,
`flags`  int(11) NOT NULL DEFAULT 0 ,
`last_login_at`  int(11) NULL DEFAULT NULL ,
`role`  enum('3','2','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '3' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `user_unique_username` (`username`) USING BTREE ,
UNIQUE INDEX `user_unique_email` (`email`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', 'lsk.hospital@gmail.com', '$2y$12$dI98GVzJ5Zyv1EJDYRcAeeuH0hlYklXZI7KiNrPggLe7bWo5zdue6', 'bylfadG2nkgPJcywRIF3fCjTuKhvNPKU', null, null, null, '::1', '1615301772', '1615301772', '0', '1624884946', '1'), ('2', 'jackie', 'jwongkamalasai@gmail.com', '$2y$12$CglYKjlIIpvITvx0r2JQteL9Db.K4nmuyXIM2LqQIBGDDVvKzD2CG', 'w5iCCaobEhcAKAQQl4invbW2wXzzU-XM', '1624792027', null, null, '::1', '1624792027', '1624792027', '0', null, '3');
COMMIT;

-- ----------------------------
-- Auto increment value for `departments`
-- ----------------------------
ALTER TABLE `departments` AUTO_INCREMENT=15;

-- ----------------------------
-- Auto increment value for `directors`
-- ----------------------------
ALTER TABLE `directors` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `office`
-- ----------------------------
ALTER TABLE `office` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `petitions`
-- ----------------------------
ALTER TABLE `petitions` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `user`
-- ----------------------------
ALTER TABLE `user` AUTO_INCREMENT=3;
